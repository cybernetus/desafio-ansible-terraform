
module "network" {
  source = "./modules/network/"
}

module "ec2" {
  source = "./modules/ec2/"
  subnet_management_id = module.network.subnet_management_id
 subnet_private_id = module.network.subnet_private_id
  subnet_public_id = module.network.subnet_public_id
  zup_bastion_sg_id = module.network.zup_bastion_sg_id
  zup_wordpress_sg_id  = module.network.zup_wordpress_sg_id
  zup_mysql_sg_id = module.network.zup_mysql_sg_id
 ami_code = var.ami_code

}
