resource "aws_instance" "zup_mysql" {
  ami = var.ami_code
  associate_public_ip_address = false
  instance_type = "t2.micro"
  subnet_id = var.subnet_private_id
  vpc_security_group_ids = ["${var.zup_mysql_sg_id}"]
  key_name = "zup-processo-seletivo"
  tags = {
    Name = "mysql_ec2"
  }
}
