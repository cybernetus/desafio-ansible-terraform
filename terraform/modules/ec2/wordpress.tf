resource "aws_instance" "wordpress_ec2" {
  ami = var.ami_code
  associate_public_ip_address = true
 instance_type = "t2.micro"
  subnet_id = var.subnet_public_id
 vpc_security_group_ids = ["${var.zup_wordpress_sg_id}"]
  key_name = "zup-processo-seletivo"

  tags = {
    Name = "wordpress_ec2"
  }
}
