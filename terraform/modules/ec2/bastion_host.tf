resource "aws_instance" "zup_bastion" {
  ami = var.ami_code
  associate_public_ip_address = true
  instance_type = "t2.micro"
  subnet_id = var.subnet_management_id
  vpc_security_group_ids = [ "${var.zup_bastion_sg_id}" ]
  key_name = "zup-processo-seletivo"
  tags = {
    Name = "zup_bastion"
  }
}



