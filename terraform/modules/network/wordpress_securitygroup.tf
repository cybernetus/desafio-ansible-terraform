resource "aws_security_group" "wordpress_securitygroup_ec2" {
  name = "wordpress_securitygroup_ec2"
  vpc_id = aws_vpc.zup_vpc.id

  tags = {
    Name = "Wordpress SecurityGroup EC2"
  }
}

resource "aws_security_group_rule" "wordpress-to-mysql" {
  type = "egress"
  from_port = 3306
  to_port = 3306
  protocol = "tcp"
  security_group_id = aws_security_group.wordpress_securitygroup_ec2.id
  source_security_group_id = aws_security_group.mysql_ec2_securitygroup.id
}

resource "aws_security_group_rule" "http_world_wordpress_ec2" {
  type = "ingress"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  # Please restrict your ingress to only necessary IPs and ports.
  # Opening to 0.0.0.0/0 can lead to security vulnerabilities
  # You may want to set a fixed ip address if you have a static ip
  security_group_id = aws_security_group.wordpress_securitygroup_ec2.id
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh-bastion-to-wordpress" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  security_group_id = aws_security_group.wordpress_securitygroup_ec2.id
  source_security_group_id = aws_security_group.zup_bastion_sg.id
}

resource "aws_security_group_rule" "zup_webserver_to_world" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.wordpress_securitygroup_ec2.id
}

output "zup_wordpress_sg_id" {
  value = aws_security_group.wordpress_securitygroup_ec2.id
}