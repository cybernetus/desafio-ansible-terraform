resource "aws_subnet" "zup_management_subnet" {
    vpc_id = aws_vpc.zup_vpc.id
    cidr_block = "10.0.8.0/24"
    map_public_ip_on_launch = "false"
    tags = {
        Name = "in_zup_management"
    }
}


output "subnet_management_id" {
  value = aws_subnet.zup_management_subnet.id
}
