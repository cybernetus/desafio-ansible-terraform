resource "aws_eip" "zup_nat" {
vpc      = true
}


resource "aws_nat_gateway" "zup_nat_gw" {
allocation_id = aws_eip.zup_nat.id
subnet_id = aws_subnet.public_subnet.id
depends_on = ["aws_internet_gateway.zup_internet_gw"]
  tags = {
    Name = "zup-nat-gw"
  }

}

# NGW
resource "aws_route_table" "ngw" {
  vpc_id = aws_vpc.zup_vpc.id

  tags = {
    Name = "zup_ngw"
  }
}

resource "aws_route" "ngw_1" {
  route_table_id = aws_route_table.ngw.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.zup_nat_gw.id
  }


