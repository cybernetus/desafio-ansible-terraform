# bastion
resource "aws_security_group" "zup_bastion_sg" {
  name = "zup-bastion-sg"
  description = "Bastion Server Zup"
  vpc_id = aws_vpc.zup_vpc.id

  tags = {
    Name = "zup_bastion_sg"
  }
}

resource "aws_security_group_rule" "ssh_world_bastion" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  security_group_id = aws_security_group.zup_bastion_sg.id
  cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh_mysql_bastion" {
  type = "egress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
 security_group_id = aws_security_group.zup_bastion_sg.id
  source_security_group_id = aws_security_group.mysql_ec2_securitygroup.id
}

resource "aws_security_group_rule" "ssh_wordpress_bastion" {
  type = "egress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  security_group_id = aws_security_group.zup_bastion_sg.id
  source_security_group_id = aws_security_group.wordpress_securitygroup_ec2.id
}

resource "aws_security_group_rule" "zup_bastion_to_world" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.zup_bastion_sg.id
}

output "zup_bastion_sg_id" {
  value = aws_security_group.zup_bastion_sg.id
}
