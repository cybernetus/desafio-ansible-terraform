resource "aws_security_group" "mysql_ec2_securitygroup" {
  name = "mysql_ec2_securitygroup"
  vpc_id = aws_vpc.zup_vpc.id

  tags = {
    Name = "Security Group Mysql Ec2"
  }
}

resource "aws_security_group_rule" "mysql-to-wordpress" {
  type = "ingress"
  from_port = 3306
  to_port = 3306
  protocol = "tcp"
  security_group_id = aws_security_group.mysql_ec2_securitygroup.id
  source_security_group_id = aws_security_group.wordpress_securitygroup_ec2.id
}

resource "aws_security_group_rule" "ssh-bastion-to-mysql" {
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  security_group_id = aws_security_group.mysql_ec2_securitygroup.id
  source_security_group_id = aws_security_group.zup_bastion_sg.id
}

resource "aws_security_group_rule" "zup_mysql_to_world" {
  type = "egress"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.mysql_ec2_securitygroup.id
}

output "zup_mysql_sg_id" {
  value = aws_security_group.mysql_ec2_securitygroup.id
}