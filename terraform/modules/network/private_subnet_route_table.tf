resource "aws_route_table_association" "private-lambda-useast-1a" {
  subnet_id = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.ngw.id
}
