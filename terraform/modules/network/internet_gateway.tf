# Internet Gateway 

resource "aws_internet_gateway" "zup_internet_gw" {
 vpc_id = aws_vpc.zup_vpc.id
 tags = {
        Name = "Zup Internet Gateway"
 }
}

