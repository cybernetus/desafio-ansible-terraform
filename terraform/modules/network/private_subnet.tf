resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.zup_vpc.id
   cidr_block = "10.0.3.0/24" 
  map_public_ip_on_launch = "false"
}

output "subnet_private_id" {
  value = aws_subnet.private_subnet.id
}
