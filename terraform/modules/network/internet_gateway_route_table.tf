resource "aws_route_table" "zup_route_table_igw" {
  vpc_id = aws_vpc.zup_vpc.id

  tags = {
    Name = "route_table_igw"
  }
}

resource "aws_route" "zup_igw_route" {
  route_table_id = aws_route_table.zup_route_table_igw.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.zup_internet_gw.id
}

