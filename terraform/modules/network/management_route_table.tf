resource "aws_route_table_association" "management_route_table" {
  subnet_id = aws_subnet.zup_management_subnet.id
  route_table_id = aws_route_table.zup_route_table_igw.id
}
