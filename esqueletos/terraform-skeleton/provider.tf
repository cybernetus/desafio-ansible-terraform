# Arquivo com o provider que será usado 
# Configuracões serão exportadas via 
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
# Provider preparado para o modelo de credenciais locais de cada 
# DevOPs, por projeto/conta
#
# shared_credentials_file = "~/.aws/credentials"
# profile                 = "projeto"

}
