resource "aws_dynamodb_table" "terraform_locks" {
  name         = "${var.nome-do-projeto}-terraform-up-and-running-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}