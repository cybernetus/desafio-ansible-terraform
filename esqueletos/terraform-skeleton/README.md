# Cookbook dos projetos da Infra/DevOps

Estes são os passos importantes para fazer um projeto em cima do **Terraform** que possa ser gerenciado por diversos analistas. A idéia é permitir que o mesmo seja inserido nas esteiras de automação ( **CI/CD** ) e também, garantir a segurança do **state** do Terraform, não permitindo que ele seja escrito por duas pessoas ao mesmo tempo e, no fim, podendo até parar um projeto inteiro. 

O projeto  **Terraform-Skeleton** contém o esqueleto para que possamos iniciar todas as iniciativas de Terraform da equipe.

Já é interessante, inclusive, que as contas do projeto na AWS já estejam criadas. Pois como é já o pontapé inicial ao começar o projeto você já pode criar esta estrutura inicial tanto na conta de **prod** quanto na conta de **devel** ( **e outras mais que sejam necessárias** ).  

Portanto, vamos aos passos. 

Neste repositório temos um projeto inicial onde iremos criar o *bucket s3* que irá hospedar o **terraform.tfstate**.

Ao dar um ls no diretório você tem os seguintes arquivos. 

```
$ ls
dynamo_lock_tfstate.tf  main.tf  provider.tf  README.md  s3_backend.tf  s3_backend_tfstate.tf  variables.tf

```

E o primeiro passo é editar o arquivo variables.tf e colocar um nome de projeto. Dando o nome do projeto de **zup-processo** o nome que usaremos é este. 

```
variable "nome-do-projeto" {

   default = "zup-processo"
}
```

Como o nosso arquivo **providers.tf** já está configurado ( *vale lembrar que caso ele tenha alguma pecularidade você deve modificá-lo para o modo como você achar necessário* ) não há mais muito o que ser feito. 

```
$ cat provider.tf
# Arquivo com o provider que será usado
# Configuracões serão exportadas via
provider "aws" {
  version = "~> 2.0"
  region  = "us-east-1"
# Provider preparado para o modelo de credenciais locais de cada
# DevOPs, por projeto/conta
#
# shared_credentials_file = "~/.aws/credentials"
# profile                 = "projeto"

}
```

Então, o segundo passo é rodar o comando `terraform init` para que o ambiente seja inicializado. 

```
$ c:/Utils/terraform/terraform.exe init

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "aws" (hashicorp/aws) 2.62.0...

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

```

Logo após devemos rodar o plan para ver se haverá algum problema. 

```
 c:/Utils/terraform/terraform.exe plan
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.


------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_dynamodb_table.terraform_locks will be created
  + resource "aws_dynamodb_table" "terraform_locks" {
      + arn              = (known after apply)
      + billing_mode     = "PAY_PER_REQUEST"
      + hash_key         = "LockID"
      + id               = (known after apply)
      + name             = "zup-terraform-up-and-running-locks"
      + stream_arn       = (known after apply)
      + stream_label     = (known after apply)
      + stream_view_type = (known after apply)

      + attribute {
          + name = "LockID"
          + type = "S"
        }

      + point_in_time_recovery {
          + enabled = (known after apply)
        }

      + server_side_encryption {
          + enabled     = (known after apply)
          + kms_key_arn = (known after apply)
        }
    }

  # aws_s3_bucket.terraform_state will be created
  + resource "aws_s3_bucket" "terraform_state" {
      + acceleration_status         = (known after apply)
      + acl                         = "private"
      + arn                         = (known after apply)
      + bucket                      = "zup-terraform-up-and-running-state"
      + bucket_domain_name          = (known after apply)
      + bucket_regional_domain_name = (known after apply)
      + force_destroy               = false
      + hosted_zone_id              = (known after apply)
      + id                          = (known after apply)
      + region                      = (known after apply)
      + request_payer               = (known after apply)
      + website_domain              = (known after apply)
      + website_endpoint            = (known after apply)

      + server_side_encryption_configuration {
          + rule {
              + apply_server_side_encryption_by_default {
                  + sse_algorithm = "AES256"
                }
            }
        }

      + versioning {
          + enabled    = true
          + mfa_delete = false
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.

```

E finalmente, iniciamos o ambiente com os dois recursos, que são o **dynamodb** que tratará o lock e o **s3** que hospedará os arquivos de state e suas versões. 

```
c:/Utils/terraform/terraform.exe apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_dynamodb_table.terraform_locks will be created
  + resource "aws_dynamodb_table" "terraform_locks" {
      + arn              = (known after apply)
      + billing_mode     = "PAY_PER_REQUEST"
      + hash_key         = "LockID"
      + id               = (known after apply)
      + name             = "zup-terraform-up-and-running-locks"
      + stream_arn       = (known after apply)
      + stream_label     = (known after apply)
      + stream_view_type = (known after apply)

      + attribute {
          + name = "LockID"
          + type = "S"
        }

      + point_in_time_recovery {
          + enabled = (known after apply)
        }

      + server_side_encryption {
          + enabled     = (known after apply)
          + kms_key_arn = (known after apply)
        }
    }

  # aws_s3_bucket.terraform_state will be created
  + resource "aws_s3_bucket" "terraform_state" {
      + acceleration_status         = (known after apply)
      + acl                         = "private"
      + arn                         = (known after apply)
      + bucket                      = "zup-terraform-up-and-running-state"
      + bucket_domain_name          = (known after apply)
      + bucket_regional_domain_name = (known after apply)
      + force_destroy               = false
      + hosted_zone_id              = (known after apply)
      + id                          = (known after apply)
      + region                      = (known after apply)
      + request_payer               = (known after apply)
      + website_domain              = (known after apply)
      + website_endpoint            = (known after apply)

      + server_side_encryption_configuration {
          + rule {
              + apply_server_side_encryption_by_default {
                  + sse_algorithm = "AES256"
                }
            }
        }

      + versioning {
          + enabled    = true
          + mfa_delete = false
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_dynamodb_table.terraform_locks: Creating...
aws_s3_bucket.terraform_state: Creating...
aws_dynamodb_table.terraform_locks: Still creating... [10s elapsed]
aws_s3_bucket.terraform_state: Still creating... [10s elapsed]
aws_dynamodb_table.terraform_locks: Creation complete after 11s [id=zup-terraform-up-and-running-locks]
aws_s3_bucket.terraform_state: Creation complete after 12s [id=zup-terraform-up-and-running-state]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

```

Feito isto, agora devemos inicializar o nosso backend **s3**, descomentando o arquivo **s3_backend.tf** e modificando para que o mesmo se pareça assim. Lembrando sempre que o nome do seu projeto deve vir no lugar onde está escrito **zup**.

E qual o motivo de não usar *variáveis* aqui ? O Terraform não aceita o uso de variáveis neste ponto, infelizmente.  

```
terraform {
 backend "s3" {
    bucket         = "zup-terraform-up-and-running-state"
    key            = "zup/s3/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "zup-terraform-up-and-running-locks"
    encrypt        = true
  }
}
```

E finalmente, rode o comando `terraform init`. 

```
c:/Utils/terraform/terraform.exe init

Initializing the backend...
Acquiring state lock. This may take a few moments...
Do you want to copy existing state to the new backend?
  Pre-existing state was found while migrating the previous "local" backend to the
  newly configured "s3" backend. No existing state was found in the newly
  configured "s3" backend. Do you want to copy this state to the new "s3"
  backend? Enter "yes" to copy and "no" to start with an empty state.

  Enter a value: yes

Releasing state lock. This may take a few moments...

Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

A partir daqui ao responder **yes** o projeto já estará utilizando o backend s3 e poderá ser colocado na esteira. 

