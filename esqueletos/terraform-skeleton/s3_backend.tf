terraform {
 backend "s3" {
    # Modifique a primeira palavra para o nome do projeto
    bucket         = "nomedoprojeto-terraform-up-and-running-state"
    key            = "zup/s3/terraform.tfstate"
    region         = "us-east-1"
    # Modifique a primeira palavra para o nome do projeto 
    dynamodb_table = "nomedoprojeto-terraform-up-and-running-locks"
    encrypt        = true
  }
}

