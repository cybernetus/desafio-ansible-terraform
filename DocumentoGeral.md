# Documento Geral 



A idéia deste documento aqui é faciltar a quem chegar neste repositório ter a condição de entender o que temos aqui. 

Como são 3 diretórios é bom explicar o que há em cada um deles e o porque de cada uma das soluções adotadas. 

## Diretório Terraform 



O diretório Terraform contém toda a estrutura necessária para **provisionar** a InfraEstrutura dentro da AWS. 

Por um hábito que acabou sendo o adotado pela equipe aqui, já que fui o primeiro a criar os modelos do Terraform eu separo os recursos por módulos. Neste caso como teríamos toda uma infra de rede ( **network** ) e uma de máquinas **ec2**, criei os dois módulos para facilitar. 

E dentro de cada diretório eu também coloco os arquivos **tf** com os nomes mais parecidos com os recursos que estão sendo provisionados pelo **Terraform**.

Com isto torna-se mais fácil a manutenção pois sempre que é necessário reconfigurar ou adicionar algo na estrutura isto fica fácil de localizar. 

No modelo terraform eu usei o **provider AWS** e sempre que preciso rodar o código ( tenho vários perfis AWS aqui configurados ) eu exporto no terminal. 

`export AWS_PROFILE=ataliba-zup`

E após isto rodo os comandos necessários : 

```HCL
$ terraform plan 
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.

module.network.aws_eip.zup_nat: Refreshing state... [id=eipalloc-01fdfd49ba4ee1764]
module.network.aws_vpc.zup_vpc: Refreshing state... [id=vpc-08ee77c031289cd95]
module.network.aws_route_table.ngw: Refreshing state... [id=rtb-0176df8d1e0d4cb5b]
module.network.aws_subnet.public_subnet: Refreshing state... [id=subnet-0226bae4e985f8d6a]
module.network.aws_route_table.zup_route_table_igw: Refreshing state... [id=rtb-09111d891ebfbecfc]
module.network.aws_internet_gateway.zup_internet_gw: Refreshing state... [id=igw-0607911a236d9ce7e]
module.network.aws_subnet.zup_management_subnet: Refreshing state... [id=subnet-012c54bdad8190ff0]
module.network.aws_subnet.private_subnet: Refreshing state... [id=subnet-0a9ebbfe647623c0c]
module.network.aws_security_group.wordpress_securitygroup_ec2: Refreshing state... [id=sg-06d6259fb8b9d34f7]
module.network.aws_security_group.zup_bastion_sg: Refreshing state... [id=sg-09c70f77517314de8]
module.network.aws_security_group.mysql_ec2_securitygroup: Refreshing state... [id=sg-0103dd85d2602a226]
module.network.aws_security_group_rule.http_world_wordpress_ec2: Refreshing state... [id=sgrule-3626218320]
module.network.aws_security_group_rule.zup_webserver_to_world: Refreshing state... [id=sgrule-2092536130]
module.ec2.aws_instance.wordpress_ec2: Refreshing state... [id=i-026a1ffec2991a5f6]
module.network.aws_security_group_rule.wordpress-to-mysql: Refreshing state... [id=sgrule-3401155853]
module.network.aws_security_group_rule.mysql-to-wordpress: Refreshing state... [id=sgrule-4066430532]
module.network.aws_security_group_rule.zup_mysql_to_world: Refreshing state... [id=sgrule-216687615]
module.network.aws_security_group_rule.ssh_wordpress_bastion: Refreshing state... [id=sgrule-2494020863]
module.network.aws_security_group_rule.ssh_world_bastion: Refreshing state... [id=sgrule-4118214863]
module.network.aws_security_group_rule.ssh-bastion-to-wordpress: Refreshing state... [id=sgrule-972363247]
module.network.aws_security_group_rule.zup_bastion_to_world: Refreshing state... [id=sgrule-3092928992]
module.network.aws_security_group_rule.ssh_mysql_bastion: Refreshing state... [id=sgrule-2987645009]
module.network.aws_security_group_rule.ssh-bastion-to-mysql: Refreshing state... [id=sgrule-2451105982]
module.network.aws_nat_gateway.zup_nat_gw: Refreshing state... [id=nat-009d2f5b9d35dfea5]
module.network.aws_route_table_association.public_route_table: Refreshing state... [id=rtbassoc-0e31956259f948c32]
module.network.aws_route_table_association.management_route_table: Refreshing state... [id=rtbassoc-0201ee8bde504b4f3]
module.network.aws_route.zup_igw_route: Refreshing state... [id=r-rtb-09111d891ebfbecfc1080289494]
module.network.aws_route_table_association.private-lambda-useast-1a: Refreshing state... [id=rtbassoc-0ec8edadf2b5327ce]
module.ec2.aws_instance.zup_bastion: Refreshing state... [id=i-08e96c637e1fbcee1]
module.ec2.aws_instance.zup_mysql: Refreshing state... [id=i-0c8a58e1db869e73b]
module.network.aws_route.ngw_1: Refreshing state... [id=r-rtb-0176df8d1e0d4cb5b1080289494]

------------------------------------------------------------------------

No changes. Infrastructure is up-to-date.

This means that Terraform did not detect any differences between your
configuration and real physical resources that exist. As a result, no
actions need to be performed.

Warning: Quoted references are deprecated

  on modules/network/nat_gateway.tf line 9, in resource "aws_nat_gateway" "zup_nat_gw":
   9: depends_on = ["aws_internet_gateway.zup_internet_gw"]

In this context, references are expected literally rather than in quotes.
Terraform 0.11 and earlier required quotes, but quoted references are now
deprecated and will be removed in a future version of Terraform. Remove the
quotes surrounding this reference to silence this warning.


Warning: Interpolation-only expressions are deprecated

  on modules/network/nat_gateway.tf line 18, in resource "aws_route_table" "ngw":
  18:   vpc_id = "${aws_vpc.zup_vpc.id}"

Terraform 0.11 and earlier required all non-constant expressions to be
provided via interpolation syntax, but this pattern is now deprecated. To
silence this warning, remove the "${ sequence from the start and the }"
sequence from the end of this expression, leaving just the inner expression.

Template interpolation syntax is still used to construct strings from
expressions when the template includes multiple interpolation sequences or a
mixture of literal strings and interpolations. This deprecation applies only
to templates that consist entirely of a single interpolation sequence.

(and 2 more similar warnings elsewhere)

```



Este código pode ser melhorado com a inserção de um s3 para gravar o *terraform.tfstate* remotamente utilizando a estrutura que é explicado esqueletos/terraform-skeleton/README.md . 

Assim utilizando aquele diretório como uma fonte inicial para o trabalho desde o início todo o estado da infraestrutura fica gravado remotamente e de forma segura na nuvem, permitindo, inclusive, que isto seja usado em uma esteira ( *apesar de durante as pesquisas para este teste vi algumas coisas interessantes relacionadas ao [tfscafold](https://faun.pub/terraform-deployments-with-aws-codepipeline-342074248843), mas não cheguei ainda a testá-lo no momento da confecção deste documento* ).

### Rede ( VPC )



A *VPC* que foi criada neste código contém a rede *10.0.0.0/16* e todas as subnets acabam seguindo este mesmo formato para permitir que possam trafegar neste barramento. 

Todas elas possuem subnets que com o CIDR /24 ( **classe C** ) que para este desafio fornece um número de ips ótimos. 

Vale lembrar que uma delas é a de gerenciamento. Como há um dispositivo nesta *subnet* que é o *Bastion Host* , ou *Jumpbox* foi necessário ligá-la diretamente ao internet gateway já que como a própria documentação da AWS fala é necessário que haja para cada dispositivo que necessite de ser visto na internet ou que precise ver a internet uma ligação ao *Internet Gateway*  da VPC. Havia na requisição o pedido de não ser ligado a um Internet Gateway. Mas eu por ter lido a documentação não achei uma forma de fazer, além desta que eu sempre segui. Se houver, realmente, será uma novidade. 

Mas acredito que a documentação fala isto, de acordo com minha interpretação. 

Uma melhora para este *Bastion* seria ativar uma porta ssh diferente e ainda, também inserir um *fail2ban* ou um *denyhosts* para que ataques de força bruta sejam rapidamente bloqueados. Esta melhoria e filtros específicos podem ser adicionados via *ansible* em um futuro. 

Este host também possui acesso completo a internet em qualquer porta. Não é o mais seguro, concordo. Mas foi o mais prático. Em um ambiente de produção o ideal seria mapear as portas necessárias ao acesso deste host na internet e liberar de forma **granular** este tráfego. 

Isto pode ser interessante pois permite no caso de um problema de intrusão na máquina que o *atacante* não consiga acesso irrestrito a rede mundial de computadores. 

 O Wordpress ( **host web** ) tem acesos também a internet irrestrito de saída  ( **egress** ) e o acesso de **ingress** ( entrada ) somente nas porta 22 vindo do host Bastion e 80 para o seu serviço de web. 

O host de banco ( mysql ) possui acesso irrestrito de saída via nat gateway para a internet e entrada nas portas 22 ( via Bastion ) e **tcp/3306** via máquina wordpress. 



## Diretório Ansible 



O diretório ansible contém os playbooks para que possam ser rodados os jobs de criação do banco de dados ( incluindo a criação de um usuário do wordpress no banco ) e outro playbook que faz a configuração de LAMP para hospedar o Wordpress. 

Pontos interessantes para uso destes playbooks. 

Ambos tem arquivos de *"configuração"* dentro do diretórios vars . O arquivo é um *yaml* que tem dados que devem ser configurados .

Como o primeiro *playbook* a ser rodado é o de criação do *mysql* devem ser configurados seguintes dados dentro do yaml . 



```yaml
mysql_root_password: "your-root-password"
mysql_db: "wpdb"
mysql_user: "wpuser"
mysql_password: "password"
mysql_host: "10.0.1.234"

```

 

- mysql_root_password: a senha de root que você quer para acesso superusuário no banco. 
- mysql_db: o banco de dados que será usado pelo seu wordpress
- mysql_user: o usuário do banco de dados do wordpress
- mysql_password: a senha do usuário do banco de dados do wordpress
- mysql_host: o host onde o wordpress está hospedado

O ip do ec2 que tem o wordpress pode ser conseguido usando o próprio terraform : 



`terraform state show module.ec2.aws_instance.wordpress_ec2`



Já no diretório do wordpress o arquivo default.yml deve ter os seguintes dados configurados. 



```yaml
#MySQL Settings
mysql_db: "wpdb"
mysql_user: "wpuser"
mysql_password: "password"
mysql_host: "10.0.3.134"

#HTTP Settings
http_host: "wp.example.com"
http_conf: "wp.example.com.conf"
http_port: "80"

```



- mysql_db: o banco de dados do wordpress ( mesmo da configuração acima )
- mysql_user: usuário do banco de dados do wordpress ( mesmo da configuração acima )
- mysql_password: senha do usuário do banco de dados do wordpress ( mesmo da configuração acima )
- mysql_host: host onde está o banco de dados ( única configuração diferente já que aqui, o ip deve ser o do host mysql )
- http_host: domínio que o seu wordpress possuirá, por exemplo, **wp.ultraman.com.br**
- http_conf: arquivo de configuração do seu wordpress 
- http_conf: porta em que o seu wordpress será publicado no mundo 



As melhoras neste processo de publicação podem ser a inseração de um *Jenkins* ou outro software para que possa ser disparado o job para criação da base de dados do wordpress e também da instalação de um wp. 

Assim pode ser criado um terceiro ansible que irá conter uma parte de criação do banco de dados ( última fase do ansible do mysql ) e a publicação do wordpress ( este completo ).

Além disto pode-se usar o packer para criar uma imagem com os usuários manager e as chaves já configuradas, evitando que seja necessário criar e copiar as chaves entre servidores. 

Também já criar um esquema para copiar o *pem* para dentro do servidor Bastion. 

Um zip contendo também este dois ansibles pode ser adicionado também e já ser copiado para dentro do ec2 durante o processo de criação da infraestrutura. 


## Monitoramento e melhorias



Apesar de não adicionado o wordpress, por ser php e também, um serviço web pode ser monitorado facilmente por qualquer APM do mercado. 

Um painel de monitoramento dos ec2 com as métricas básicas como disco, rede, CPU etc deve ser adicionado também relacionado a cada uma das EC2. 

Como é um Wordpress o cloudfront pode ser uma ótima opção para hospedar os arquivo estáticos do mesmo. Adicionar um cloudfront no modelo do terraform pode ser uma boa opção. 

# Materiais e documentos acessados 

## Videos

- [Deploy Sample Application (WordPress) to AWS EC2 using GitLab Pipeline Template](https://www.youtube.com/watch?v=17xY8HzzM1c)
- [Dynamic cloud infra with packer + terraform + ansible + jenkins: Mehul Ved, Nexsales Solution](https://www.youtube.com/watch?v=jHRy8lMsjfs)
- [How to use Ansible to automate your infrastructure](https://www.youtube.com/watch?v=J1oOCmixgH4)
- [Infrastructure DevSecOps using Ansible and Jenkins](https://www.youtube.com/watch?v=yLQSPWhlz6g)
- [Ansible, Jenkins, and AWS Integration | Ansible Series Part 3](https://www.youtube.com/watch?v=BaSiFwvGwgg)
- [Easily deploy Wordpress with Ansible](https://www.youtube.com/watch?v=qKTHnALDpEU)
- [How to Deploy a WordPress Site with Ansible in Minutes](https://www.youtube.com/watch?v=NR7KrsCb8CY)

## Textos

- [Ansible and Jenkins - Orchestration and Configuration](https://crmtrilogix.com/Cloud-Blog/Dev-Test-Optimisation/Ansible-and-Jenkins---Orchestration-and-Configuration/266)
- [How teams use GitLab and Terraform for infrastructure as code: A demo](https://about.gitlab.com/topics/gitops/gitlab-enables-infrastructure-as-code/)
- [Usecase: GitOps](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/)
- [Gitlab hero border pattern right svg
Multicloud deployment for GitOps using GitLab: A demo](https://about.gitlab.com/topics/gitops/gitops-multicloud-deployments-gitlab/)
- [How to Use Ansible to Install and Set Up LAMP on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-use-ansible-to-install-and-set-up-lamp-on-ubuntu-18-04)
- [How to Install and Set Up WordPress Using Ansible on CentOS 8](https://linuxbuz.com/linuxhowto/how-to-install-wordpress-with-ansible)
- [How to Deploy a Jenkins Cluster on AWS as Part of a Fully Automate CI/CD Platform](https://blog.sonatype.com/how-to-deploy-a-jenkins-cluster-on-aws-as-part-of-a-fully-automate-ci/cd-platform)
- [Learning DevOps: Aws, Terraform, Ansible, Jenkins, and Docker](https://www.scottyfullstack.com/blog/devops-01-aws-terraform-ansible-jenkins-and-docker/)
- [Ansible :: [WARNING]: Module did not set no_log for update_password](https://stackoverflow.com/questions/60720775/ansible-warning-module-did-not-set-no-log-for-update-password)
- [A simple MariaDB deployment with Ansible](http://youdidwhatwithtsql.com/simple-mariadb-deployment-ansible/2311/)
- [Gist nginx ](https://gist.github.com/sshymko/1a9467487a895a7dc8af2c101d7a9c22)
- [ANSIBLE - CREATING AN EC2 INSTANCE & ADDING KEYS TO AUTHORIZED_KEYS](https://www.bogotobogo.com/DevOps/Ansible/Ansible-aws-creating-ec2-instance.php)

