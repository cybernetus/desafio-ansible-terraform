# Diretorio com todos os source codes do Desafio


Este diretório contém todos os arquivos usados para resolução do desafio. 


Dividi em diretório para ficar mais fácil entender. 

Portanto. 

- terraform - contém os códigos utilziados para a infraestrutura 
- ansible - contém os diretório **mysql** e **wordpress** que contém todos os arquivos necessários para colocar o wordpress em funcionamento. 

Ainda há pontos que podem ser melhorados neste projeto. Uma parte do mesmo para fazer já a criação da zona e **host** que irá responder na internet por este wordpress. 

O arquivo [DocumentoGeral.md](./DocumentoGeral.md) contém uma explicação rápida de como usar os códigos aqui contidos. 


